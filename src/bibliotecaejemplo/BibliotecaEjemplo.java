/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaejemplo;
import com.mitlley.biblioteca.ejemplo.vistas.jframes.Main;

/**
 *
 * @author mitlley
 * @version 0.0.1 29 de Junio de 2017
 * 
 * El objetivo de esta clase es poder reaizar pruebas
 * en las clases presentes en la capa Modelo.
 */
public class BibliotecaEjemplo {

    /**
     * @param args the command line arguments
     * 
     * La ejecucion de este proyecto consiste en probar
     * la correcta programacion de las clases POJO de la
     * capa Modelo.
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
    
}
