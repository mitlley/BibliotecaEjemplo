/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitlley.biblioteca.ejemplo.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mitlley
 */
public class ClienteModelo {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    public ClienteModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaEjemploPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    public Cliente crearCliente(String nombre, String apellidoPaterno, String apellidoMaterno){
        // Agregar los datos del cliente
        Cliente cliente = new Cliente();
        cliente.setNombre(nombre);
        cliente.setApellidoPaterno(apellidoPaterno);
        cliente.setApellidoMaterno(apellidoMaterno);
        
        // Guardarlos y confirmar en la base de datos
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(cliente);
        this.entityManager.getTransaction().commit();
        
        return cliente;
    }  
    
    public boolean eliminarClientePorId(int idCliente){
        // Encontrar la venta
        Cliente cliente = this.entityManager.find(Cliente.class, idCliente);
        
        // Eliminar
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(cliente);
        this.entityManager.getTransaction().commit();
        return true;
    }
    
    public List<Cliente> listaClientes(){
        List<Cliente> lista = this.entityManager.createQuery("SELECT c FROM Cliente c").getResultList();
        
        return lista;
    }
    
    public Cliente porId(int id){
        Cliente cliente = this.entityManager.find(Cliente.class, id);
        
        return cliente;
    }
    
}
