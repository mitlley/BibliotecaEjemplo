/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitlley.biblioteca.ejemplo.modelos;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mitlley
 */
public class VentaModelo {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    public VentaModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaEjemploPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    public Venta registrarVenta(int idCliente, Date fecha, int monto){
        // Agregar los datos de la venta
        Venta venta = new Venta();
        venta.setIdCliente(idCliente);
        venta.setFecha(fecha);
        venta.setMonto(monto);
        
        //Guardamos y confirmamos
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(venta);
        this.entityManager.getTransaction().commit();
        
        return venta;
    }
    
    public boolean eliminarVentaPorId(int idVenta){
        // Encontrar la venta
        Venta venta = this.entityManager.find(Venta.class, idVenta);
        
        // Eliminar
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(venta);
        this.entityManager.getTransaction().commit();
        return true;
    }
    
}
