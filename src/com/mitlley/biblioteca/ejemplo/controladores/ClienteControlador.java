/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitlley.biblioteca.ejemplo.controladores;

import com.mitlley.biblioteca.ejemplo.modelos.Cliente;
import com.mitlley.biblioteca.ejemplo.modelos.ClienteModelo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mitlley
 */
public class ClienteControlador {
    
    private ClienteModelo clienteModelo;

    public ClienteControlador() {
        clienteModelo = new ClienteModelo();
    }
    
    public List<Cliente> listaClientes(){
        List<Cliente> lista = this.clienteModelo.listaClientes();
        
        return lista;
    }
    
    public Cliente crearCliente(String nombre, String apellidoPaterno, String apellidoMaterno){
        Cliente cliente = this.clienteModelo.crearCliente(nombre, apellidoPaterno, apellidoMaterno);
        
        return cliente;
    }
    
    public Cliente porId(int id){
        Cliente cliente = this.clienteModelo.porId(id);
        
        return cliente;
    }
}
